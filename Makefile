all: build

build:
	docker compose build --no-cache
run: 
frontend:
	docker build --add-host frontend:127.0.0.1 -t frontend:1.0 -f dockerfiles/frontend.dockerfile . 
proxy:
	docker build --add-host proxy:127.0.0.1 -t proxy:1.0 -f dockerfiles/nginx.dockerfile . 

run:
	docker run -d -p 3000:3000 frontend:1.0
	docker run -d -p 80:80 proxy:1.0


restore:
	docker exec products-showcase-mongo-1 mongosh --eval 'use productShowcase; db.createUser({user: "rek3000", pwd: "thuanlp123", roles: [ { role: "readWrite", db: "productShowcase" } ]})'
	docker exec products-showcase-mongo-1 mongorestore -u rek3000 -p thuanlp123 --db productShowcase /data/db/backup/productShowcase
