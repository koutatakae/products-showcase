FROM node:22-alpine3.19 AS base
WORKDIR /src
COPY frontend/package.json .
COPY frontend/public ./public
COPY frontend/src ./src
# COPY frontend/.env .
COPY frontend/tsconfig.json .
RUN npm install

FROM base AS dev
CMD ["npm", "start"]


FROM base AS build
RUN npm run build


FROM nginx:1.27-alpine AS prod
WORKDIR /var/www

COPY --from=build /src/build/. /var/www/html/
COPY configs/frontend/frontend-prod.conf /etc/nginx/conf.d/
COPY configs/frontend/nginx.conf /etc/nginx/
RUN chown -R nginx:nginx /var/www/html && \
        chmod -R 755 /var/www/html && \
        chown -R nginx:nginx /var/cache/nginx && \
        chown -R nginx:nginx /var/log/nginx && \
        chown -R nginx:nginx /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid

USER nginx
CMD ["nginx", "-g", "daemon off;"]
