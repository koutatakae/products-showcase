# FROM mongodb/mongodb-community-server:7.0-ubuntu2204
FROM mongo

# ARG MONGODB_USERNAME
# ARG MONGODB_PASSWORD
# ARG MONGODB_DATABASE

# RUN mongosh --eval 'use productShowcase; db.createUser({user: $MONGODB_USERNAME, pwd: $MONGODB_PASSWORD, roles: [ { role: "readWrite", db: $MONGODB_DATABASE" } ] })'
# RUN mongorestore --db productShowcase /root/dump
# RUN chown -R mongodb:mongodb /data/db


CMD ["mongod"]
