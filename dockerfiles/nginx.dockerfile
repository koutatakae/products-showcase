FROM nginx:1.27-alpine AS base
WORKDIR /var/www
RUN touch /var/run/nginx.pid && \
        chown -R nginx:nginx /var/run/nginx.pid

COPY configs/fullchain.pem /etc/ssl/certs
COPY configs/privkey.pem /etc/ssl/
COPY configs/nginx.conf /etc/nginx/
RUN rm /etc/nginx/conf.d/default.conf
RUN   chown -R nginx:nginx /var/cache/nginx && \
        chown -R nginx:nginx /var/log/nginx && \
        chown -R nginx:nginx /etc/nginx && \
        chown nginx:nginx /etc/ssl/privkey.pem && \
        chown nginx:nginx /etc/ssl/certs/fullchain.pem

FROM base AS dev
COPY configs/proxy/proxy-dev.conf /etc/nginx/conf.d/
RUN chown -R nginx:nginx /etc/nginx/conf.d
USER nginx
CMD ["nginx", "-g", "daemon off;"]

FROM base AS prod
COPY configs/proxy/proxy-prod.conf /etc/nginx/conf.d/
RUN chown -R nginx:nginx /etc/nginx/conf.d
USER nginx
CMD ["nginx", "-g", "daemon off;"]
