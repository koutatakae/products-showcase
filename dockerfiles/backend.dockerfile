FROM amazoncorretto:17-alpine3.17 AS base
WORKDIR /src
COPY backend/mvnw .
COPY backend/.mvn ./.mvn
COPY backend/pom.xml .
COPY backend/src ./src
RUN  ./mvnw dependency:resolve

FROM base AS dev
WORKDIR /src
RUN cp /src/src/main/resources/application-dev.yaml \
      /src/src/main/resources/application.yaml
CMD ["./mvnw", "spring-boot:run"] 

FROM base AS build
WORKDIR /src
RUN cp /src/src/main/resources/application-prod.yaml \
      /src/src/main/resources/application.yaml
RUN ./mvnw clean install -Dmaven.test.skip=true

FROM amazoncorretto:17-alpine3.17 AS prod
RUN addgroup -S spring && adduser -S spring -G spring
WORKDIR /var/app
COPY --from=build /src/target/hanu-0.0.1-SNAPSHOT.jar /var/app/
RUN chown -R spring:spring /var/app
USER spring

CMD ["java", "-jar", "hanu-0.0.1-SNAPSHOT.jar"]
