pipeline {
  environment {
    SECRET_FILE_COMPOSE = credentials('product-compose')
    SECRET_FILE_FRONTEND = credentials('product-frontend')
    SECRET_FILE_FRONTEND_DEV = credentials('product-frontend-dev')
  }
  options {
    gitLabConnection('products-showcase-connection')
  }
  agent any
  stages {
    stage('Build') {
      steps {
        echo "Building packages Development Environment"
        sh("cp $SECRET_FILE_FRONTEND_DEV ./frontend/.env")
        sh("cp $SECRET_FILE_COMPOSE .env")
        sh("docker-compose -f compose-dev.yml build")
        updateGitlabCommitStatus name: 'Build', state: 'success'
      }
    }
    stage('Test') {
      steps {
        echo "Testing database"
        updateGitlabCommitStatus name: 'Test', state: 'success'
      }
    }
    stage('Deploy to Development') {
      when {
        expression {
          currentBuild.result == null || currentBuild.result == 'SUCCESS'
        }
      }
      steps {
        echo "Deploying to Development server"
        sh("docker-compose -f compose-dev.yml up -d")
        echo "Deployed as Docker Compose"
        updateGitlabCommitStatus name: 'Deploy to Development', state: 'success'
      }
    }
    stage('Integration Test') {
      steps {
        echo "Integration test ..."
        // sh("curl -k https://mock:444/api/category/find-by-parent-id?parentId=6618e3aa3cc2dd10315fe620")
        updateGitlabCommitStatus name: 'Integration Test', state: 'success'
      }
    }
    stage('Build and push for Production') {
      when {
        expression { currentBuild.currentResult == 'SUCCESS' }
      }
      steps {
        echo "Building packages - Development Environment"
        sh("cp $SECRET_FILE_FRONTEND ./frontend/.env")

        script {
          docker.withRegistry('https://637423262460.dkr.ecr.us-east-1.amazonaws.com', 'ecr:us-east-1:aws-credentials') {
            def frontend = docker.build("pcase-frontend:${env.BUILD_ID}", "-f dockerfiles/frontend.dockerfile .")
            def backend = docker.build("pcase-backend:${env.BUILD_ID}", "-f dockerfiles/backend.dockerfile .")
            def proxy = docker.build("pcase-proxy:${env.BUILD_ID}", "-f dockerfiles/nginx.dockerfile .")
            def mongo = docker.build("pcase-mongo:${env.BUILD_ID}", "-f dockerfiles/mongodb.dockerfile .")

            frontend.push()
            frontend.push('latest')
            backend.push()
            backend.push('latest')
            proxy.push()
            proxy.push('latest')
            mongo.push()
            mongo.push('latest')
          }
        }
        // sh("docker build --target prod -t frontend:${env.BUILD_ID} -f dockerfiles/frontend.dockerfile .")
        // sh("docker build --target prod -t backend:${env.BUILD_ID} -f dockerfiles/backend.dockerfile .")
        // sh("docker build --target prod -t proxy:${env.BUILD_ID} -f dockerfiles/nginx.dockerfile .")
        // sh("docker build --target prod -t mongo:${env.BUILD_ID} -f dockerfiles/nginx.dockerfile .")
        updateGitlabCommitStatus name: 'Build and push for Production', state: 'success'
      }
    }
    stage('Deploy to Production') {
      // dependsOn 'Build for Production'
      steps {
        script {
          docker.withRegistry('https://637423262460.dkr.ecr.us-east-1.amazonaws.com', 'ecr:us-east-1:aws-credentials') {
            def frontend = docker.image("pcase-frontend:latest")
            def backend = docker.image("pcase-backend:latest")
            def proxy = docker.image("pcase-proxy:latest")
            def mongo = docker.image("pcase-mongo:latest")
            frontend.pull()
            backend.pull()
            proxy.pull()
            mongo.pull()
          }
        }
        // updateGitlabCommitStatus name: 'Deploy for Production', state: 'pending'
        echo "Deploying to prod server"
        sh("docker-compose -f compose-prod.yml up -d")
        updateGitlabCommitStatus name: 'Deploy for Production', state: 'success'
      }
    }
  }
  post {
    success {
      updateGitlabCommitStatus state: 'success'
    }
    aborted {
      updateGitlabCommitStatus state: 'canceled'
    }
    failure {
      sh("docker-compose -f compose-dev.yml down")
      //sh("docker system prune -af")
      updateGitlabCommitStatus state: 'failed'
    }
  }
}
