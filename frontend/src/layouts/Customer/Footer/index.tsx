import zalo from "assets/images/Zalo.png";
import facebook from "assets/images/Facebook.png";
import app from "assets/images/app.png";
import logo from "assets/images/logo.jpg";
import "./Footer.css";

export function Footer() {
  return (
    <footer className="footer-container text-white row " style={{ backgroundImage: "linear-gradient(to right, #0b3963, #12426f, #184b7c, #1d5488, #66abc9)" }}>
      <div className="d-flex navbar-brand col-md-2 justify-content-center align-items-center">
        <img className="w-100 m-4  d-none d-md-block" src={logo} alt="Logo" />
      </div>

      <div className="col-md-5 pt-4 justify-content-center align-items-center" style={{ textAlign: "start" }}>
        <h2 className="fw-bold text-white">CÔNG TY TNHH THIẾT BỊ ĐIỆN MINH TÚ </h2>
        <h5 className="bold">Địa chỉ: Cụm CN xã Trung Thành, Huyện Vụ Bản, Tỉnh Nam Định</h5>
        <h5 className="bold">Số điện thoại: Mr Tuấn - 0944088800</h5>
        <h5 className="bold">Email: Congtyminhtu.vbnd@gmail.com</h5>
      </div>
      <div className="d-flex col-md-5 mb-3 justify-content-center align-items-end">
        <a href="http://zalo.me/0944088800"><img src={zalo} alt="Zalo" className="rounded " /></a>
        <a href="https://www.facebook.com/congtyminhtu.vbnd/"><img src={facebook} alt="Facebook" className="rounded" /></a>
      </div>

    </footer>
  );
}
